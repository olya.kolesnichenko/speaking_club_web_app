

### Install Dependencies


```
npm install
```



### Run the Application


```
npm start
```

[You can click here](http://localhost:8000/)
Now browse to the app at [`localhost:8000/`]
