(function () {

    'use strict';

    var app = angular
        .module('speaking_club')
        .controller('ChatController', ['$scope', "$stateParams", '$rootScope', 'UsersService', 'ChatService', ChatController]);

    function ChatController($scope, $stateParams, $rootScope, UsersService, ChatService) {

        var chatId = $stateParams.chat_id;
        $scope.messages = {};
        $scope.message = {};
        $scope.message.text = "";
        ChatService.getMessages().then(function (data) {
            $scope.messages = data;
        });
        $scope.sendMessage = function (message) {
            if (message.id == null) {
                $scope.addMessage(message);
            }
            else
                $scope.updateMessage(message);
        };
        $scope.addMessage = function (message) {

            $scope.message = {};
            $scope.message.text = "";
            ChatService.newMessage(message);
            updateScroll();
        };

        $scope.editMessage = function (message) {
            $scope.message = message;


        };
        $scope.updateMessage = function (message) {
            $scope.message = message;
            $scope.message = {};
            $scope.message.text = "";
            ChatService.updateMessage(message);
            updateScroll();

        };
        //bind to notifyOnNewMessageFromServer
        $rootScope.$on('chat:newMessage', function (event, data) {
            //console.log(event, data);
            playAudio();
            updateScroll();
            console.log(data);

            if (data.user == null) {
                UsersService.getUser(data.user_id).then(function (user) {
                    data.user = user;

                    $scope.messages.push(data);
                });

            }
            else {
                $scope.messages.push(data);
            }

        });
        $rootScope.$on('chat:updateMessage', function (event, data) {
            //console.log(event, data);

            for (var i = 0; i < $scope.messages.length; i++)
                if ($scope.messages[i].id == data.id) {
                    $scope.messages[i] = data;
                    break;
                }

        });
        $scope.currentUserId = function () {
            $scope.user_id = 1; /// TODO  get in Session ?
            return $scope.user_id;

        };
        function playAudio() {
            var audio = new Audio('web/audio/chat.mp3');
            audio.play();
        }

        function updateScroll(){
            setTimeout(function () {
                var element = document.getElementById("chat_window");

                element.scrollTop = element.scrollHeight - element.clientHeight + 100;
            }, 100);

        }
    }


})();