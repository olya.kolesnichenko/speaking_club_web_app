/**
 * Created by ovi on 27/08/16.
 */

(function() {

    var app = angular
        .module('speaking_club')
        .controller('LoginController', ['$scope','$state', 'AuthorizeService', LoginController]);
    function LoginController($scope, $state, AuthorizeService) {

        $scope.user = {
            email: "admin@admin.com",
            password: "123456"
        };

        $scope.submitForm = function() {

            $scope.error = "";
            AuthorizeService.postSignIn($scope.user).then(function (res) {
                $state.go("index");
            }, function (error) {
                $scope.error = error;
            });

        };

    }

}());