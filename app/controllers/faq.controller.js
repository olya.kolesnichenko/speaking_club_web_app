/**
 * Created by ovi on 27/08/16.
 */

(function () {

    'use strict';

    var app = angular
        .module('speaking_club')
        .controller('FaqController', ['$scope', 'FaqService', FaqController]);

    function FaqController($scope, FaqService) {

        $scope.url = document.location.href;
        FaqService.getFaqClient().then(function (data) {
            $scope.faqs = data;
        });

    }
})();