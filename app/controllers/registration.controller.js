(function () {


    var app = angular
        .module('speaking_club')
        .controller('RegistrationController', ['$scope', '$state', 'AuthorizeService', RegistrationController]);

    function RegistrationController($scope, $state, AuthorizeService) {



        $scope.user = {
            id: 55,
            email: "admin@admin.com",
            nickName: "Fdfdsfds",
            password: "111111",
            confirm_password: "111111"
        };

        $scope.submitForm = function () {

            $scope.error = "";
            AuthorizeService.postRegistration($scope.user).then(function (res) {

                console.log(res);
                $state.go("index");
            }, function (error) {
                console.log(error);
                $scope.error = error;
            });


        };

    }

    var compareTo = function () {
        return {
            require: "ngModel",
            scope: {
                otherModelValue: "=compareTo"
            },
            link: function (scope, element, attributes, ngModel) {

                ngModel.$validators.compareTo = function (modelValue) {
                    return modelValue == scope.otherModelValue;
                };

                scope.$watch("otherModelValue", function () {
                    ngModel.$validate();
                });
            }
        };
    };

    app.directive("compareTo", compareTo);


}());