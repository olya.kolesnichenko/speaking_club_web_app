/**
 * Created by ovi on 27/08/16.
 */


(function () {

    'use strict';

    var app = angular
        .module('speaking_club')
        .controller('AboutController', ['$scope', 'AboutService', AboutController]);

    function AboutController($scope, AboutService) {

        AboutService.getAbout().then(function (data) {

            $scope.about = data;
        });

    }
})();