(function () {

    'use strict';

    var app = angular
        .module('speaking_club')
        .controller('FeedbackController', ['$scope', '$state', 'FeedbackService', FeedbackController]);
    var MAX_LEN = 200;
    var WARN_LEN = 10;
    var user_id = localStorage.getItem("token");
    function FeedbackController($scope, $state, FeedbackService) {


        $scope.feedback_data = {};
        $scope.feedback_data.user_id= user_id;
        $scope.feedback_data.text= "";


        console.log($scope.feedback_data);
        $scope.remaining = function () {

            return (MAX_LEN - $scope.feedback.text.$viewValue.length) || 0;
        };
        $scope.shouldWarn = function () {
            console.log('warn');
            console.log($scope.remaining());
            return WARN_LEN > $scope.remaining();
        };


        $scope.submitForm = function () {
            $scope.error = "";
            // check to make sure the form is completely valid
            if ($scope.feedback.$valid) {
              $scope.feedback.name = $scope.feedback_data.name;
                alert('feedback has been sent');
               // FeedbackService.newFeedback( $scope.user);
               FeedbackService.postFeedback($scope.feedback).then(function (res) {

                    console.log(res);
                    console.log($scope.feedback_data);
                   $scope.feedback_data = {};
                   console.log($scope.feedback_data);
                    $state.go("index");
                }, function (error) {
                    console.log(error);
                    $scope.error = error;
                });

            }

        };


    }
})();