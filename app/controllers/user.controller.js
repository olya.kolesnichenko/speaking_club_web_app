/**
 * Created by Sunny on 04.04.2017.
 */
(function () {

    'use strict';

    var app = angular
        .module('speaking_club')
        .controller('UserController', ['$scope', "$stateParams", 'UsersService', UserController]);

    function UserController($scope, $stateParams, UsersService) {

        var userId = $stateParams.user_id;

        UsersService.getUser(userId).then(function (data) {
            $scope.user = data;
        });

    }
})();