/**
 * Created by Sunny on 14.04.2017.
 */
(function () {

    'use strict';

    var app = angular
        .module('speaking_club')
        .controller('LogoutController', ['$scope', '$state', 'AuthorizeService', LogoutController]);

    function LogoutController($scope, $state, AuthorizeService) {

        $scope.logout = function(){

            AuthorizeService.logOut().then(function (res) {
                $state.go("index");
            }, function (error) {
                $state.go("index");  ///TODO
            });

        };

    }
})();