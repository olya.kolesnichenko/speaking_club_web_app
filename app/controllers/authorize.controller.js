/**
 * Created by Sunny on 04.04.2017.
 */
(function () {

    'use strict';

    var app = angular
        .module('speaking_club')
        .controller('AuthorizeController', ['$scope', 'AuthorizeService', AuthorizeController]);

    function AuthorizeController($scope, AuthorizeService) {

        $scope.$watch(function () {
            return AuthorizeService.isAuthorized();
        }, function () {
            $scope.authorized = AuthorizeService.isAuthorized();

        });
    }
})();