/**
 * Created by Sunny on 04.04.2017.
 */
(function () {

    'use strict';

    var app = angular
        .module('speaking_club')
        .controller('UsersController', ['$scope', 'UsersService', UsersController]);

    function UsersController($scope, UsersService) {

        UsersService.getAllUsers().then(function (data) {
            $scope.users = data;
        });

    }
})();