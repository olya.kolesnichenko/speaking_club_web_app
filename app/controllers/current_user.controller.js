/**
 * Created by Sunny on 04.04.2017.
 */
(function () {

    'use strict';

    var app = angular
        .module('speaking_club')
        .controller('CurrentUserController', ['$scope', 'AuthorizeService', '$filter', CurrentUserController]);

    function CurrentUserController($scope, AuthorizeService, $filter) {


        $scope.user = {};
        AuthorizeService.getCurrentUser().then(function (data) {
            $scope.user = data;
        });
  
        $scope.genders = [
            {value: 2, text: 'female'},
            {value: 1, text: 'male'}
        ];

        $scope.showGender = function () {
            var selected = $filter('filter')($scope.genders, {value: $scope.user.gender});
            return ($scope.user.gender && selected.length) ? selected[0].text : 'Not set';
        };
    }
})();