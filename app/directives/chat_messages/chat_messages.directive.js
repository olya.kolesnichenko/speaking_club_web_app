/**
 * Created by Sunny on 11.04.2017.
 */

(function () {

    'use strict';

    var app = angular.module("speaking_club");
    app.directive("chatMessagesDirective", function() {
        return {
            restrict: 'AE',
            replace: 'true',
            templateUrl: './directives/chat_messages/chat_messages.directive.html'
        };
    });

})();