
(function () {

    'use strict';

    var app = angular.module("speaking_club");
    app.directive("userDirective", function() {
        return {
            restrict: 'AE',
            replace: 'true',
            templateUrl: './directives/user/user.directive.html'
        };
    });

})();