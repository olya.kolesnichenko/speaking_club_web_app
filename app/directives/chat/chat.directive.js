
(function () {

    'use strict';

    var app = angular.module("speaking_club");
    app.directive("chatDirective", function() {
        return {
            restrict: 'AE',
            replace: 'true',
            templateUrl: './directives/chat/chat.directive.html'
        };
    });

})();