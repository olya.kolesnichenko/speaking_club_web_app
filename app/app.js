'use strict';


angular.module('speaking_club', ['ui.router', "ngSanitize", 'xeditable', 'mgcrea.ngStrap', 'angular-smilies'])
    .config(routesConfig);

angular.module('speaking_club').run(function(editableOptions) {
    editableOptions.theme = 'bs3';
});
function routesConfig($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("/");
    $stateProvider
        .state('index', {
            url: '/',
            templateUrl: '/views/site/index.tpl.html',
            controller: 'IndexController',
            controllerAs: 'vm'
        })
        .state("about", {
            url: '/about',
            templateUrl: "/views/site/about.tpl.html",
            controller: 'AboutController',
            controllerAs: 'vm'
        })
        .state("faq", {
            url: '/faq',
            templateUrl: "/views/site/faq.tpl.html",
            controller: 'FaqController',
            controllerAs: 'vm'
        })

        .state("login", {
            url: '/login',
            templateUrl: "/views/site/login.tpl.html",
            controller: 'LoginController',
            controllerAs: 'vm'
        })
        .state("registration", {
            url: '/registration',
            templateUrl: "/views/site/registration.tpl.html",
            controller: 'RegistrationController',
            controllerAs: 'vm'
        })
        .state("feedback", {
            url: '/feedback',
            templateUrl: "/views/site/feedback.tpl.html",
            controller: 'FeedbackController',
            controllerAs: 'vm'
        })
        .state("chat", {
            url: '/chat/:chat_id',
            templateUrl: "/views/site/chat.tpl.html",
            controller: 'ChatController',
            controllerAs: 'vm'
        })
        .state("edituser", {
            url: '/edituser',
            templateUrl: "/views/site/edituser.tpl.html",
            controller: 'CurrentUserController',
            controllerAs: 'vm'
        })
        .state("user", {
            url: '/user/:user_id',
            templateUrl: "/views/site/user.tpl.html",
            controller: 'UserController',
            controllerAs: 'vm'
        });

}