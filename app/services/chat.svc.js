/**
 * Created by Sunny on 27.01.17.
 */
(function () {
    'use strict';
    angular.module('speaking_club')
        .factory('ChatService', ['$rootScope', '$http', ChatService]);

    function ChatService($rootScope, $http) {
        var messageId = new Date().getMilliseconds();

        setTimeout(function () {
            var msg = {};
            msg.id = messageId++;
            msg.text = "text" + msg.id;
            msg.user_id = 1;
            msg.time = new Date();

            emitNewMessage(msg)
        }, 1000);

        return {
            emitNewMessage: emitNewMessage,
            newMessage: newMessage,
            emitUpdateMessage: emitUpdateMessage,
            updateMessage: updateMessage,
            getMessages: getMessages
        };


        function emitNewMessage(message) {
            console.log(message);

            $rootScope.$emit("chat:newMessage", message);
        }

        function newMessage(msg) {
            msg.id = messageId++;
            msg.user_id = 1;
            msg.time = new Date();
            console.log(msg);
           // instead emting should be post to the server  TODO
            emitNewMessage(msg);

        }

        function emitUpdateMessage(message) {
            console.log(message);
            $rootScope.$emit("chat:updateMessage", message);
        }

        function updateMessage(message) {

            // instead emting should be post to the server  TODO
            emitUpdateMessage(message);

        }

        function getMessages() {
            return $http.get('data/messages.json').then(function (res) {
                return res.data;
            });
        }


    }
})();