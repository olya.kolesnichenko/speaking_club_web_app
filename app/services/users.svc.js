/**
 * Created by Sunny on 04.04.2017.
 */
(function () {
    'use strict';
    angular
        .module('speaking_club')
        .factory('UsersService', ['$http', '$q', UsersService]);

    function UsersService($http, $q, $window) {

        var cachedUsers = {};

        return {
            getAllUsers: getAllUsers,
            getUser: getUser
        };

        function getAllUsers() {
            return $http.get('/data/users.json').then(function (res) {
                return res.data;
            });
        }

        function getUser(id) {
            if (cachedUsers[id] == null) {
                return $http.get('/data/user.json').then(function (res) {
                    cachedUsers[id] = res.data;
                    return res.data;
                });
            }
            else {
                var deferred = $q.defer();
                deferred.resolve(cachedUsers[id]);
                return deferred.promise;
            }
        }
    }
})();