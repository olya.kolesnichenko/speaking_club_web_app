/**
 * Created by Sunny on 07.04.2017.
 */
(function () {
    'use strict';
    angular
        .module('speaking_club')
        .factory('AuthorizeService', ['$http', '$window', "$q", AuthorizeService]);

    function AuthorizeService($http, $window, $q) {
        var authorized;
        console.log(localStorage.getItem("token"));
        if (localStorage.getItem("token") != undefined && localStorage.getItem("token") != null) {
            $http.defaults.headers.common.Authorization = 'Bearer ' + localStorage.getItem("token");
            authorized = true;
        }
        else {
            authorized = false;
        }

        return {
            isAuthorized: isAuthorized,
            getCurrentUser: getCurrentUser,
            postRegistration: postRegistration,
            postSignIn: postSignIn,
            logOut: logOut
        };


        function isAuthorized() {
            return authorized;
        }

        function postRegistration(user) {
            // perform some asynchronous operation, resolve or reject the promise when appropriate.
            return $q(function (resolve, reject) {
                setTimeout(function () {
                    console.log(localStorage.getItem("token"));
                    if (user.email == 'admin@admin.com') {
                        // $window.localStorage["token"] = user.token;
                        //$http.defaults.headers.common.Authorization = 'Bearer ' + user.token; TODO
                        console.log(user.id);
                        console.log(localStorage.getItem("token"));
                        localStorage.setItem("token", user.id);
                        console.log(localStorage.getItem("token"));
                        $http.defaults.headers.common.Authorization = 'Bearer ' + localStorage.getItem("token");
                        authorized = true;
                        resolve(true);
                    } else {
                        reject("ivalide email");
                    }
                }, 1000);

            });
        }

        function postSignIn(user) {
            // perform some asynchronous operation, resolve or reject the promise when appropriate.
            return $q(function (resolve, reject) {
                setTimeout(function () {
                    console.log(localStorage.getItem("token"));
                    if (user.email == "admin@admin.com" && user.password == "123456") {
                        // $window.localStorage["token"] = user.token;
                        //$http.defaults.headers.common.Authorization = 'Bearer ' + user.token; TODO
                        localStorage.setItem("token", user.id);
                        $http.defaults.headers.common.Authorization = 'Bearer ' + ', hi!' + localStorage.getItem("token");
                        authorized = true;
                        resolve(true);

                    } else {
                        reject("ivalide email or password");
                    }
                }, 1000);

            });
        }

        function logOut() {
            // perform some asynchronous operation, resolve or reject the promise when appropriate.
            return $q(function (resolve, reject) {
                setTimeout(function () {
                    console.log(localStorage.getItem("token"));
                    if (localStorage.getItem("token") != undefined) {
                        authorized = false;
                        localStorage.clear();
                        resolve(true);

                    } else {
                        reject("logout error");
                    }
                }, 1000);

            });
        }

        function getCurrentUser() {
            return $http.get('/data/user.json').then(function (res) {
                return res.data;
            });
        }


    }
})();