/**
 * Created by Sunny on 27.01.17.
 */
(function () {
    'use strict';
    angular.module('speaking_club')
        .factory('AboutService', ['$http', AboutService]);

    function AboutService($http, $window) {

        return {
            getAbout: getAbout,
            getAboutCooperation: getAboutCooperation


        };

        function getAbout() {
            return $http.get('data/about.json').then(function (res) {
                return res.data;
            });
        }

        function getAboutCooperation() {
            return $http.get('data/about_cooperation.json').then(function (res) {
                return res.data;
            });
        }


    }
})();