(function () {
    'use strict';
    angular.module('speaking_club')
        .factory('FeedbackService', ['$rootScope', '$http', "$q", FeedbackService]);

    function FeedbackService($rootScope, $http, $q ) {

        return {
            postFeedback: postFeedback,
            emitNewFeedback: emitNewFeedback,
            newFeedback: newFeedback

        };


        function postFeedback(feedback) {
            // perform some asynchronous operation, resolve or reject the promise when appropriate.
            return $q(function(resolve, reject) {
                setTimeout(function() {
                    if (feedback.name.length>0) {
                        resolve(true);
                    } else {
                        reject(false);
                    }
                }, 1000);
            });
        }
        function emitNewFeedback(feedback) {
            console.log(feedback);
            $rootScope.$emit("feedback:newFeedback", feedback);
        }

        function newFeedback(feedback) {

            // instead emting should be post to the server  TODO
            emitNewFeedback(feedback);

        }

    }
})();